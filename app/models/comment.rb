class Comment < ActiveRecord::Base
  belongs_to :post
  # added a comment
  validates_presence_of :post_id
  validates_presence_of :body
end
